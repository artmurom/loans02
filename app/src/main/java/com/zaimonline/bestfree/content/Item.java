package com.zaimonline.bestfree.content;

import android.os.Parcel;
import android.os.Parcelable;

public class Item implements Parcelable {

    private String logo;
    private String name;
    private String shortDesc1;
    private String shortDesc2;
    private String shortDesc3;
    private String link;
    private String fullDesc;

    private Item(Parcel in) {
        logo = in.readString();
        name = in.readString();
        shortDesc1 = in.readString();
        shortDesc2 = in.readString();
        shortDesc3 = in.readString();
        link = in.readString();
        fullDesc = in.readString();
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    public String getLogo() {
        return logo;
    }

    public String getName() {
        return name;
    }

    public String getShortDesc1() {
        return shortDesc1;
    }

    public String getShortDesc2() {
        return shortDesc2;
    }

    public String getShortDesc3() {
        return shortDesc3;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getFullDesc() {
        return fullDesc;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(logo);
        dest.writeString(name);
        dest.writeString(shortDesc1);
        dest.writeString(shortDesc2);
        dest.writeString(shortDesc3);
        dest.writeString(link);
        dest.writeString(fullDesc);
    }
}

