package com.zaimonline.bestfree.repository;


import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.zaimonline.bestfree.content.Data;
import com.zaimonline.bestfree.content.Item;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class ItemRepository {

    private static final String TAG = "ItemRepository";

    private static final String JSON_FILE = "loans.json";

    private static final String REF = "http://zcredit00.pro/go.php?articles=fppruocmpizcxlhyrd55&r=%s";


    public ItemRepository() {
    }

    public static List<Item> getItemList(Context context) {
        String json = "";
        try {
            InputStream is = context.getAssets().open(JSON_FILE);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            Log.e(TAG, " error load assets");
        }

        Gson gson = new Gson();
        Data data = gson.fromJson(json, Data.class);

        List<Item> itemList = data.getItemList();

        for (Item item:itemList) {
            String link = String.format(REF, item.getLink());
            item.setLink(link);
        }

        return data.getItemList();
    }
}
