package com.zaimonline.bestfree.screen.detail;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.zaimonline.bestfree.R;
import com.zaimonline.bestfree.content.Item;
import com.zaimonline.bestfree.screen.main.AccessToMainView;

import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DetailFragment extends Fragment {

    private static final String TAG = "DetailFragment";

    private static final String EXTRA_ITEM = "item";

    @BindView(R.id.ivLogo)
    ImageView mIvLogo;

    @BindView(R.id.tvDescription)
    TextView mTvDescription;

    @BindView(R.id.btnGetMoney)
    Button mBtnGetMoney;

    private Context mContext;

    private Item mItem;

    public DetailFragment() {
    }

    public static DetailFragment newInstance(Item item) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_ITEM, item);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mItem = getArguments().getParcelable(EXTRA_ITEM);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);

        ButterKnife.bind(this, rootView);

        initUI();

        return rootView;
    }


    private void initUI() {
        try {
            String imagePath = "image/" + mItem.getLogo();
            InputStream ims = mContext.getAssets().open(imagePath);
            Drawable d = Drawable.createFromStream(ims, null);
            mIvLogo.setImageDrawable(d);
            ims.close();
        } catch (IOException ex) {
            Log.e(TAG, " error load assets");
        }

        mTvDescription.setText(Html.fromHtml(mItem.getFullDesc()));
        mBtnGetMoney.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mItem.getLink()));
            startActivity(intent);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mContext instanceof AccessToMainView) {
            ((AccessToMainView) mContext).setVisibleBackButton(true);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mContext instanceof AccessToMainView) {
            ((AccessToMainView) mContext).setVisibleBackButton(false);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }
}
