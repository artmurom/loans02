package com.zaimonline.bestfree.screen.main;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.zaimonline.bestfree.R;
import com.zaimonline.bestfree.screen.common.RootFragment;
import com.zaimonline.bestfree.screen.loans.LoansFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements AccessToMainView {

    private static final String TAG = "MainActivity";

    @BindView(R.id.tabLayout)
    TabLayout mTabLayout;

    @BindView(R.id.viewPager)
    ViewPager mViewPager;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    private boolean mIsFragmentWithBackButtonActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);


        mToolbar.setNavigationOnClickListener((view) -> onBackPressed());

        String[] titleArray = getResources().getStringArray(R.array.main_tabs_title);

        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), titleArray);

        mViewPager.setAdapter(myPagerAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position==0) {
                    mToolbar.setNavigationIcon(null);
                } else {
                    if (mIsFragmentWithBackButtonActive) {
                        mToolbar.setNavigationIcon(R.drawable.ic_back);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mTabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public void setVisibleBackButton(boolean isVisible) {
        mIsFragmentWithBackButtonActive = isVisible;
        if (isVisible) {
            mToolbar.setNavigationIcon(R.drawable.ic_back);
        } else {
            mToolbar.setNavigationIcon(null);
        }
    }

    public static class MyPagerAdapter extends FragmentPagerAdapter {

        private static int NUM_ITEMS = 2;
        private static String[] TITLE_ARRAY;

        MyPagerAdapter(FragmentManager fm, String[] titleArray) {
            super(fm);
            TITLE_ARRAY = titleArray;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            switch (position + 1) {
                case 1:
                    fragment = new LoansFragment();
                    break;
                case 2:
                    fragment = new RootFragment();
                    break;
                default:
                    fragment = new LoansFragment();
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLE_ARRAY[position];
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


}
