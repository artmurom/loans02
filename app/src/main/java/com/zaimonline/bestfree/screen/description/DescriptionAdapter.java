package com.zaimonline.bestfree.screen.description;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.zaimonline.bestfree.R;
import com.zaimonline.bestfree.content.Item;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


class DescriptionAdapter extends RecyclerView.Adapter<DescriptionAdapter.LoansHolder> {

    private static final String TAG = "DescriptionAdapter";

    private final List<Item> mItemList;
    private final OnItemClick mOnItemClick;

    private final View.OnClickListener mInternalListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Item item = (Item) view.getTag();
            mOnItemClick.onItemClick(item);
        }
    };

    DescriptionAdapter(OnItemClick onItemClick, List<Item> itemList) {
        mItemList = itemList;
        mOnItemClick = onItemClick;
    }


    @NonNull
    @Override
    public LoansHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.item_description, parent, false);
        return new LoansHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull LoansHolder holder, int position) {
        Item item = mItemList.get(position);
        holder.bind(item);
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    class LoansHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivLogo)
        ImageView mIvLogo;

        @BindView(R.id.btnRead)
        Button mBtnRead;


        LoansHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Item item) {

            try {
                String imagePath = "image/" + item.getLogo();
                InputStream ims = itemView.getContext().getAssets().open(imagePath);
                Drawable d = Drawable.createFromStream(ims, null);
                mIvLogo.setImageDrawable(d);
                ims.close();
            } catch (IOException ex) {
                Log.e(TAG, " error load assets");
            }

            mBtnRead.setTag(item);
            mBtnRead.setOnClickListener(mInternalListener);
        }
    }


    interface OnItemClick {

        void onItemClick(Item item);

    }
}
