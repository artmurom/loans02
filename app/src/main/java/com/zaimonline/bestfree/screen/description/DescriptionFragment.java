package com.zaimonline.bestfree.screen.description;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zaimonline.bestfree.R;
import com.zaimonline.bestfree.content.Item;
import com.zaimonline.bestfree.repository.ItemRepository;
import com.zaimonline.bestfree.screen.detail.DetailFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DescriptionFragment extends Fragment implements DescriptionAdapter.OnItemClick {

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private Context mContext;

    public DescriptionFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_loans, container, false);

        ButterKnife.bind(this, rootView);

        List<Item> itemList = ItemRepository.getItemList(mContext);

        DescriptionAdapter descriptionAdapter = new DescriptionAdapter(this, itemList);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

        mRecyclerView.setAdapter(descriptionAdapter);

        return rootView;
    }

    @Override
    public void onItemClick(Item item) {
        FragmentTransaction trans = getFragmentManager()
                .beginTransaction();
        trans.replace(R.id.rootFrame, DetailFragment.newInstance(item));
        trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        trans.addToBackStack(null);
        trans.commit();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }
}
