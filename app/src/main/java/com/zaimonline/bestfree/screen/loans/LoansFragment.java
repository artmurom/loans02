package com.zaimonline.bestfree.screen.loans;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zaimonline.bestfree.R;
import com.zaimonline.bestfree.content.Item;
import com.zaimonline.bestfree.repository.ItemRepository;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class LoansFragment extends Fragment implements LoansAdapter.OnItemClick {


    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private Context mContext;

    public LoansFragment() {
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_loans, container, false);

        ButterKnife.bind(this, rootView);

        List<Item> itemList = ItemRepository.getItemList(mContext);

        LoansAdapter loansAdapter = new LoansAdapter(this, itemList);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));

        mRecyclerView.setAdapter(loansAdapter);

        return rootView;
    }

    @Override
    public void onItemClick(String link) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        startActivity(browserIntent);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mContext = null;
    }
}
